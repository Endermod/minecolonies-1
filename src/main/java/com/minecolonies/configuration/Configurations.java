package com.minecolonies.configuration;

public class Configurations
{
    /*
    Defaults
     */
    public static final int     DEFAULT_WORKINGRANGETOWNHALL = 10; //TODO make legit
    public static final boolean DEFAULT_ALLOWINFINTESUPPLYCHESTS = true;

    /*
    Holders
     */
    public static int workingRangeTownhall;
    public static String[] cityNames = {"myCity", "yourCity"}; //TODO add names
    public static boolean allowInfiniteSupplyChests;
}
