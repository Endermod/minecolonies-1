package com.minecolonies.proxy;

public interface IProxy
{
    void registerTileEntities();

    void registerEntities();

    void registerEntityRendering();
}
